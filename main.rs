extern crate image;
extern crate num;

use image::RgbImage;
use num::complex::Complex64;

const CANVAS_HEIGHT: usize = 2000;
const CANVAS_WIDTH: usize = 2000;

fn mandelbrot_value(x: f64, y: f64, max_iterations: u8) -> u8 {
    let mut n = 0;

    let c: Complex64 = Complex64::new(x, y);
    
    let mut z = 0.0 + c;
    
    return loop {
        z = z*z + c;

        if z.norm_sqr() > (max_iterations as f64 * max_iterations as f64) {
            break n * (255/max_iterations);
        }
        
        if n == max_iterations {
            break 255;
        }
        n = n + 1;
    }
}


fn julia_value(x: f64, y: f64, max_iterations: u8, cr: f64, ci: f64) -> u8 {
    let mut n = 0;

    let c: Complex64 = Complex64::new(cr, ci);
    
    let mut z = Complex64::new(x, y);
    
    return loop {
        z = z*z + c;

        if z.norm_sqr() > (max_iterations as f64) {
            break n * (255/max_iterations);
        }
        
        if n == max_iterations {
            break 255;
        }
        n = n + 1;
    }
}


fn get_distance(a: i16, b: i16) -> u8 {
    if a > b {
        return (a - b) as u8
    } else {
        return (b - a) as u8
    }
}

    
fn get_color(growth: u8) -> [u8; 3] {
    let r: u8;
    let g: u8;
    let b: u8;

    let r_cs: i16 = 128;
    let g_cs: i16 = 64;
    let b_cs: i16 = 191;

    r = get_distance(growth as i16, r_cs);
    g = get_distance(growth as i16, g_cs);
    b = get_distance(growth as i16, b_cs);

    return [r, g, b];
}


fn generate_bitmap(canvas: [[u8; CANVAS_WIDTH]; CANVAS_HEIGHT]) -> RgbImage {
    let mut img = RgbImage::new(CANVAS_HEIGHT as u32, CANVAS_WIDTH as u32);
    let mut growth: u8;
    
    for row in 0..(CANVAS_HEIGHT - 1) {        
        for column in 0..(CANVAS_WIDTH - 1) {
            growth = canvas[row][column];
            img.get_pixel_mut(row as u32, column as u32).data = get_color(growth);
        }
    }

    return img;
}


fn main() {
    let max_x: f64 = 2;
    let max_y: f64 = 2;
    let min_x: f64 = -2;
    let min_y: f64 = -2;

    let x_stepsize: f64 = (max_x - min_x) / CANVAS_HEIGHT as f64;
    let y_stepsize: f64 = (max_y - min_y) / CANVAS_WIDTH as f64;
        
    let mut canvas = [[0u8; CANVAS_WIDTH]; CANVAS_HEIGHT];

    println!("Calculating set...");
    
    for row in 0..(CANVAS_HEIGHT - 1) {        
        for column in 0..(CANVAS_WIDTH - 1) {
            canvas[row][column] = mandelbrot_value(min_x + row as f64 * x_stepsize, max_y - column as f64 * y_stepsize, 100);
        }
    }

    println!("Finished set.");
    println!("Generating bitmap...");
    
    let img = generate_bitmap(canvas);
    
    println!("Bitmap finished.");
    println!("Saving...");
    
    img.save("fractal.bmp").unwrap();

    println!("File saved.")
}
